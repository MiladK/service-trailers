const pkg = require('./package.json')
const config = require('./config')
const app = require('./src/app')

app.listen(config.APP_PORT, () => {
  console.log(`${pkg.name} is listening to http://localhost:${config.APP_PORT}`)
})
