module.exports = (err, req, res, next) => {
  res.status(err.statusCode || /* istanbul ignore next */ 500)
    .json({
      error: err.name,
      message: err.message
    })
}
