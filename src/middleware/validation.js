const Joi = require('joi')

const validateOptions = {
  convert: false
}

// I didn't write a test that validate anything
const validationMiddlewareFactory = (/* istanbul ignore next */ querySchema = null, bodySchema = null) => async (req, res, next) => {
  try {
    const promises = []

    // I didn't write a test that doesn't validate query
    /* istanbul ignore next */
    if (querySchema) {
      promises.push(querySchema.validateAsync(req.query, validateOptions))
    }

    /*
    // If we want to validate body in POST requests too
    if (bodySchema) {
      promises.push(bodySchema.validateAsync(req.body, validateOptions))
    }
    */

    await Promise.all(promises)

    next()
  } catch (err) {
    err.statusCode = 400

    next(err)
  }
}

const trailersRequestSchema = Joi.object({
  // If we have several valid URL patterns, we can test them here
  resource: Joi.string()
    .pattern(/https:\/\/content.viaplay.se\/pc-(se|no)\/film\/([a-z0-9])+\/?/)
    .error(new Error('`resource` needs to be a valid Viaplay resource URL!'))
    .required()
}).unknown(true)

module.exports = {
  trailersRequestValidation: validationMiddlewareFactory(trailersRequestSchema)
}
