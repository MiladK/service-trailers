const express = require('express')
const health = require('./handlers/health')
const trailerUrls = require('./handlers/trailerUrls')
const trailerUrlsCache = require('./handlers/trailerUrlsCache')
const errorHandler = require('./middleware/errorHandler')
const {
  trailersRequestValidation
} = require('./middleware/validation')

const app = express()
app.disable('x-powered-by')

app.get('/health', health)
app.get('/api/v1/trailers', trailersRequestValidation, trailerUrlsCache, trailerUrls)

app.use(errorHandler)

module.exports = app
