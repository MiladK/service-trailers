const health = (req, res, next) => {
  res.set('Cache-Control', 'no-store')

  res.json({ status: 'healthy' })
}

module.exports = health
