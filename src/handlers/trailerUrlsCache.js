const { getAsync } = require('../providers/redisCache')

const get = async (req, res, next) => {
  try {
    const data = await getAsync(req.query.resource)

    if (data) {
      res.json(JSON.parse(data))
    } else {
      // No data found, proceed to fetch the information normally!
      next()
    }
  } catch (err) {
    // If we fail to get the cache from redis for any reason, let's fetch the information normally
    /* istanbul ignore next */
    next()
  }
}

module.exports = get
