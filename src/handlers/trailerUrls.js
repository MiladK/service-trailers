const axios = require('axios')
const {
  getMovieTrailer
} = require('../providers/tmdb')
const {
  setAsync
} = require('../providers/redisCache')

const get = async (req, res, next) => {
  try {
    // We already validated that req.query.resource is a valid Viaplay resource, now we can call it safely
    const response = await axios.get(req.query.resource)

    // As per the task document, this is where we get the imdb id
    // We will believe this blindly for now
    const { id } = response.data._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb

    // Ask TMDB for trailers info!
    const data = await getMovieTrailer(id)

    const trailers = (data.trailers && data.trailers.youtube)
      ? data.trailers.youtube
          .filter(trailer => trailer.type === 'Trailer') // Remove non-trailer clips
          .map(trailer => `https://www.youtube.com/watch?v=${trailer.source}`)
      : /* istanbul ignore next */ []

    const finalResponse = {
      trailers
    }

    await setAsync(req.query.resource, JSON.stringify(finalResponse))
      .catch(console.error) // I want to tolerate caching errors, hide errors from clients and log them.

    res.json(finalResponse)
  } catch (err) {
    next(err)
  }
}

module.exports = get
