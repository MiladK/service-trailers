const { promisify } = require('util')
const redis = require('redis')
const config = require('../../config')

/**
 * @see https://github.com/NodeRedis/node-redis#options-object-properties
 * @type {RedisClient}
 */
const client = redis.createClient({
  host: config.REDIS_HOST,
  port: config.REDIS_PORT,
  connect_timeout: 400
})

client.on('error', console.error)
client.on('warning', console.warn)

const delAsync = promisify(client.del).bind(client)
const getAsync = promisify(client.get).bind(client)
const setAsync = promisify(client.set).bind(client)

module.exports = {
  delAsync,
  getAsync,
  setAsync
}
