const axios = require('axios')
const config = require('../../config')

const httpBaseRequest = (method, url) => {
  const options = {
    method,
    baseURL: config.THE_MOVIE_DB_BASEURL,
    url,
    headers: {
      Authorization: `Bearer ${config.THE_MOVIE_DB_BEARER_TOKEN}`,
      Accept: 'application/json'
    }
  }

  return axios.request(options)
    .then(res => res.data)
}

const httpGetRequest = url => httpBaseRequest('get', url)

const getMovieTrailer = id => httpGetRequest(`/movie/${id}?append_to_response=trailers`)

module.exports = {
  getMovieTrailer
}
