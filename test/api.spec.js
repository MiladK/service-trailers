const assert = require('assert')
const {
  after,
  afterEach,
  before,
  beforeEach,
  describe,
  it
} = require('mocha')
const mockery = require('mockery')
const nock = require('nock')
const redisMock = require('redis-mock')
const supertest = require('supertest')
const data = require('./test.json')

const resource = 'https://content.viaplay.se/pc-se/film/arrival-2016'

// Mock the request to the Viaplay movie resource link
function mockViaplayRequest () {
  return nock('https://content.viaplay.se')
    .get('/pc-se/film/arrival-2016')
    .once()
    .reply(200, data['viaplay-arrival-2016'])
}

function mockTMDBRequest () {
  return nock('https://api.themoviedb.org')
    .get('/3/movie/tt2543164')
    .query({ append_to_response: 'trailers' })
    .once()
    .reply(200, data['tmdb-tt2543164'])
}

describe('API', function () {
  let request

  before(function () {
    this.timeout(2000)

    mockery.registerMock('redis', redisMock)

    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false
    })

    const app = require('../src/app')
    request = supertest(app)
  })

  describe('Security', function () {
    /** @see http://expressjs.com/en/advanced/best-practice-security.html#at-a-minimum-disable-x-powered-by-header */
    it('does NOT reveal Express as our framework', function (done) {
      request.get('/health')
        .expect(200)
        .expect(function (res) {
          if (Object.prototype.hasOwnProperty.call(res.headers, 'x-powered-by')) {
            throw new Error('API reveals the Express library')
          }
        })
        .end(done)
    })
  })

  describe('GET /health', function () {
    const url = '/health'

    it('responds with 200 HTTP Code', function (done) {
      request.get(url)
        .expect(200)
        .end(done)
    })

    it('responds with JSON Content-Type', function (done) {
      request.get(url)
        .expect('Content-Type', /json/)
        .end(done)
    })

    it('sends an appropriate response body', function (done) {
      request.get(url)
        .expect({ status: 'healthy' })
        .end(done)
    })
  })

  describe('GET /api/v1/trailers', function () {
    const url = '/api/v1/trailers'

    beforeEach(function () {
      nock.cleanAll()
    })

    describe('Validation', function () {
      it('accepts Viaplay movie resource link', function (done) {
        mockViaplayRequest()
        mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .expect(200)
          .end(done)
      })

      it('rejects non-Viaplay movie resource link', function (done) {
        request.get(url)
          .query({ resource: 'https://www.google.com/' })
          .expect(400)
          .expect({
            error: 'Error',
            message: '`resource` needs to be a valid Viaplay resource URL!'
          })
          .end(done)
      })
    })

    describe('Reading Viaplay Resource', function () {
      it('reads Viaplay movie resource link', function (done) {
        const scope = mockViaplayRequest()
        mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .end(function () {
            // Is the request scope satisfied?
            scope.done()

            done()
          })
      })
    })

    describe('Getting Trailers from TMDB', function () {
      it('requests movie info from TMDB', function (done) {
        mockViaplayRequest()
        const scope = mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .end(function () {
            // Is the request scope satisfied?
            scope.done()

            done()
          })
      })
    })

    describe('Error Handling', function () {
      it('handles errors in Viaplay resource link', function (done) {
        nock('https://content.viaplay.se')
          .get('/pc-se/film/arrival-2016')
          .once()
          .reply(500)
        mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .expect(500)
          .expect({
            error: 'Error',
            message: 'Request failed with status code 500'
          })
          .end(done)
      })

      it('handles errors in tmdb', function (done) {
        mockViaplayRequest()
        nock('https://api.themoviedb.org')
          .get('/3/movie/tt2543164')
          .query({ append_to_response: 'trailers' })
          .once()
          .reply(500)

        request.get(url)
          .query({ resource })
          .expect(500)
          .expect({
            error: 'Error',
            message: 'Request failed with status code 500'
          })
          .end(done)
      })
    })

    describe('When Everything Goes Well', function () {
      it('returns an array of trailer links', function (done) {
        mockViaplayRequest()
        mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .expect(200)
          .expect(data.our_response)
          .end(done)
      })
    })

    describe('With Redis Cache', function () {
      before(function () {
        // We want to make sure that redis cache server has a cached copy of our request/response
        const { setAsync } = require('../src/providers/redisCache')
        return setAsync(resource, JSON.stringify(data.our_response))
      })

      it('serves response from cache', function (done) {
        // We want to make sure that both the movie resource URL and TMDB are not requested, because we want to serve from the cache
        const scope1 = mockViaplayRequest()
        const scope2 = mockTMDBRequest()

        request.get(url)
          .query({ resource })
          .expect(data.our_response) // And let's confirm that the cache served the same expected result
          .end(function () {
            // If the scopes are not satisfied that means we served from the cache
            assert.strictEqual(scope1.isDone(), false)
            assert.strictEqual(scope2.isDone(), false)

            done()
          })
      })
    })

    afterEach(function () {
      // Clean the cache so it doesn't interfere with testing the code that doesn't use caching
      const { delAsync } = require('../src/providers/redisCache')
      return delAsync(resource)
    })
  })

  after(function () {
    mockery.disable()
    mockery.deregisterMock('redis')
  })
})
