module.exports = {
  asyncOnly: true,
  checkLeaks: true,
  color: true,
  diff: true,
  forbidOnly: true,
  forbidPending: true,
  globals: ['__coverage__'],
  slow: 1000,
  timeout: 500,
}
