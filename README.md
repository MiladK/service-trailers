# service-trailers
A microservice for providing clients with trailer URLs. The microservice takes a movie resource link (e.g. https://content.viaplay.se/pc-se/film/arrival-2016) as input and based on that returns the URL to the trailer for that movie.

## Scaling
How do we handle heavy load (tens of thousands of requests per second)?

- In service solution: Since a movie resource URL would result in the same list of trailers most of the time, then we can add a cache server like `redis`, which can optimize the microservice, reduce the load and reduce the response time for each request from 100s ms to 2-10 ms.
- We can deploy several instances of this service and put them behind a load balancer.
- We could serve the microservice via Amazon CloudFront which is capable of caching API requests and responses. In such a case, I'd build this microservice as a serverless AWS Lambda function using AWS SAM. This is the cheapest solution in terms of computing resource, and therefore financially.

## Installation
- Clone the repo `git clone git@gitlab.com:MiladK/service-trailers.git`
- Install dependencies `npm ci`
- Copy your .env file `cp .env.example .env` and fill the appropriate values, especially `THE_MOVIE_DB_BEARER_TOKEN` that you can get from [here](https://www.themoviedb.org/settings/api) and `REDIS_HOST`.
- Run the service `npm run start`

## Testing
Clone the repo, install the dependencies `npm ci` and then run `npm run test`

## Running Using docker-compose
Inside the directory, run `docker-compose up --build`

When ready, then visit `http://localhost:3000/api/v1/trailers?resource=https://content.viaplay.se/pc-se/film/arrival-2016`

## Running in a Docker Container
The docker image is optimized using [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/).

Assuming that you have a running Redis server, then the steps to running a docker container are:
- Build the image `docker build . -t service-trailers`
  - You can define the build environment using `--build-arg` as `docker build . -t service-trailers --build-arg NODE_ENV=production`
- Create `.env` file as explained in the [installation instructions](#installation) because we want to mount it to the counter as a volume.
- Run a container `docker run -d --network host -v /full/path/to/.env:/opt/app/.env --name service-trailers-container service-trailers`
  - You can define the runtime environment using `--env` as `docker run -d --env NODE_ENV=production --network host -v /full/path/to/.env:/opt/app/.env --name service-trailers-container service-trailers`

## Scripts
- Linting: I prefer to source out my code style problem. That's why I use [JavaScript Standard Style](https://standardjs.com/). Run `npm run lint` to check for code style errors, or `npm run lint:fix` to fix those that can be fixed automatically.
- Testing: Run `npm run test` and `npm run test:watch` for running the tests while developing.
- Test Coverage: Run `npm run test:coverage` to see the test coverage of the code. (It's not really 100%).

## Things Left Out Intentionally and ToDOs
- Authentication and Authorization: We possibly want to authenticate our clients and make sure they have permission to use this service.
- Securing the connection to Redis server.
- I'd have also built this using AWS SAM instead of a traditional node.js process.
