FROM node:14-alpine AS building_stage

# Define work directory
WORKDIR /opt/app

# Declare an argument. ARG is only available during the build process.
ARG NODE_ENV=development

# Copy package files
COPY package*.json ./

# Install 3rd-party dependencies
RUN npm ci --progress=false

# Copy the rest of the files (better to define what we need instead of using .dockerignore)
COPY config ./config
COPY src ./src
COPY index.js ./index.js

FROM node:14-alpine

# Define work directory
WORKDIR /opt/app

# Copy the build
COPY --from=building_stage /opt/app .

# Declare ENV argument
ENV NODE_ENV=development

# Expose port
EXPOSE 3000

# Start App
CMD ["node", "index.js"]
