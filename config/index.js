const config = require('exp-config')

config.APP_PORT = parseInt(config.APP_PORT, 10)
config.REDIS_PORT = parseInt(config.REDIS_PORT, 10)

/**
 * @type {{
 *   APP_PORT: number,
 *   REDIS_HOST: string,
 *   REDIS_PORT: number,
 *   THE_MOVIE_DB_BASEURL: string,
 *   THE_MOVIE_DB_BEARER_TOKEN: string,
 * }}
 */
module.exports = config
